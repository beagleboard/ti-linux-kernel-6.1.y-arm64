#!/bin/bash

cd ./linux/

#patch -p1 < ../patches/mikrobus/0001-dt-bindings-misc-Add-mikrobus-connector.patch
#patch -p1 < ../patches/mikrobus/0002-w1-eeprom-changes.patch
#patch -p1 < ../patches/mikrobus/0003-spi-Make-of_find_spi_controller_by_node-available.patch
#patch -p1 < ../patches/mikrobus/0004-serdev-add-of_-helper-to-get-serdev-controller.patch
#patch -p1 < ../patches/mikrobus/0005-mikrobus-Add-mikrobus-driver.patch
#patch -p1 < ../patches/mikrobus/0006-dts-ti-k3-am625-beagleplay-Add-mikroBUS.patch

#patch -p1 < ../patches/0001-arm64-dts-ti-k3-am625-beagleplay-Use-pwrseq-for-TI-s.patch
#patch -p1 < ../patches/0002-k3-am625-beagleplay-wl18xx-use-10-ms.patch

patch -p1 < ../patches/0001-arm64-dts-ti-k3-am625-beagleplay-Use-mmc-pwrseq-for-.patch

git diff > changes.txt ; mv ./changes.txt ../public/changes.txt

cd ../
